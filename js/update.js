(function(w) {
	var wgtVer = null;
	var version = null;

	function plusReady() {
		// ......  
		// 获取本地应用资源版本号  
		plus.runtime.getProperty(plus.runtime.appid, function(inf) {
			wgtVer = inf.version;
			console.log("当前应用版本：" + wgtVer);
			plus.storage.setItem("version", wgtVer);
			checkUpdate();
		});
	}
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener('plusready', plusReady, false);
	}

	// 检测更新  
	var checkUrl = config.config.path + "/config/getVersion";

	function checkUpdate() {
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			switch (xhr.readyState) {
				case 4:
					plus.nativeUI.closeWaiting();
					if (xhr.status == 200) {
						console.log("检测更新成功：" + xhr.responseText);
						var newVer = xhr.responseText;
						if (newVer == '暂无app版本信息') return;
						version = newVer.split("@")[0];
						var updateNotice = newVer.split("@")[1];
						if(updateNotice){
							updateNotice = "更新内容：\n" + updateNotice;
						}else{
							updateNotice = " "; 
						}
						if (wgtVer && version && (wgtVer != version)) {
							plus.nativeUI.confirm(updateNotice, function(i) {
								if (0 == i.index) {
									downWgt(); // 下载升级包
								} else {
									return;
								}
							}, "新版本 v" + version, ["立即更新", "取　　消"]);
						} else {}
					} else {
						console.log("检测更新失败！");
					}
					break;
				default:
					break;
			}
		}
		xhr.open('GET', checkUrl);
		xhr.send();
	}

	function checkUpdate2() {
		plus.nativeUI.showWaiting("检测更新...");
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			switch (xhr.readyState) {
				case 4:
					plus.nativeUI.closeWaiting();
					if (xhr.status == 200) {
						console.log("检测更新成功：" + xhr.responseText);
						var newVer = xhr.responseText.split("\n")[0];
						if (newVer == '暂无app版本信息') {
							plus.nativeUI.alert("无新版本可更新！");
							return;
						}
						if (wgtVer && newVer && (wgtVer != newVer)) {
							plus.nativeUI.confirm("新版本 v" + newVer, function(i) {
								if (0 == i.index) {
									downWgt(); // 下载升级包
								} else {
									return;
								}
							}, "是否更新", ["立即更新", "取　　消"]);
						} else {
							plus.nativeUI.alert("无新版本可更新！");
						}
					} else {
						console.log("检测更新失败！");
						plus.nativeUI.alert("检测更新失败！");
					}
					break;
				default:
					break;
			}
		}
		xhr.open('GET', checkUrl);
		xhr.send();
	}

	// 下载wgt文件  
	var wgtUrl = config.config.path + "/config/update.wgt";

	function downWgt() {
		plus.nativeUI.showWaiting("下载资源文件...");
		plus.downloader.createDownload(wgtUrl, {
			filename: "_doc/update/"
		}, function(d, status) {
			if (status == 200) {
				console.log("下载资源成功：" + d.filename);
				installWgt(d.filename); // 安装wgt包
			} else {
				console.log("下载资源失败！");
				plus.nativeUI.alert("下载资源失败！");
			}
			plus.nativeUI.closeWaiting();
		}).start();
	}

	// 更新应用资源  
	function installWgt(path) {
		plus.nativeUI.showWaiting("安装资源文件...");
		plus.runtime.install(path, {}, function() {
			plus.nativeUI.closeWaiting();
			console.log("安装资源文件成功！");
			plus.nativeUI.alert("更新完成！", function() {
				plus.storage.setItem("version", version);
				plus.runtime.restart();
			});
		}, function(e) {
			plus.nativeUI.closeWaiting();
			console.log("安装资源文件失败[" + e.code + "]：" + e.message);
			plus.nativeUI.alert("安装资源文件失败[" + e.code + "]：" + e.message);
		});
	}


})(window);
